import { app } from "./app";
import { cfg } from "./cfg";
import { initDB } from "./db";
import { logger } from "./logger";

async function start() {

    await initDB();

    app.listen(cfg.port, () => {
        logger.info(`Server listening on ${ cfg.port }`);
    })
    
}

start().catch((err) => {
    logger.error(`FATAL starting app: `, err);
});

process.on('unhandledRejection', (reason, promise) => {
    logger.error('Unhandled Rejection', reason, promise);
    // Application specific logging, throwing an error, or other logic here
});
// process.on('beforeExit', async () => {
    // await db?.close();
    // logger.info(`DB Closed successfully`);
// });
